# -*- coding: utf-8 -*-
"""
Created on Sat Jun  2 12:27:14 2018

@author: enovi
"""

import nltk
#Social Dominance Analyzer

def main():
    choice = True
    article_text = ''
    c = 'y'
    print('Welcome to News Analyzer!')
    while choice == True:
         c = input('Analyze an article y/n? ')
         if c == 'y':
             article_text = get_text()
             analyze_text(article_text)
             choice = True
         elif c == 'n':
             choice = False
             
def get_text():
    article_text = ''
    article_text = str(input('Enter the message here: '))
    return article_text

def analyze_text(article_text):
    #high and low social dominance
    high_dom = [('a','talent'), ('be','decisive'), ('be','obedient'), ('be','successful'),
                ('best','talent'), ('better','than'), ('hard','work'), ('high','status'),
                ('high','prestige'), ('in','charge'), ('inferior','to'), ('kept','in'),
                ('kept','down'), ('lose','the'), ('low','prestige'), ('low','status'), 
                ('natural','difference'), ('of','success'), ('talent','in'),
                ('the','bottom'), ('the','loser'), ('the','best'), ('the','disagreement'),
                ('the','elite'), ('the','fight'), ('the','military'), ('the','talent'),
                ('the','time'), ('the','top'), ('the','winner'), ('their','place'),
                ('they','deserve'), ('they','obey'), ('threaten','to'), ('through','threats'),
                ('through','submission'), ('top','tier'), ('to','fail'), ('to','intimidate'),
                ('to','obey'), ('to','punish'), ('to','remind'), ('to','sacrifice'),
                ('to','restrict'), ('to','understand'), ('to','us'), ('seize','control'),
                ('should','agree'), ('should','believe'), ('should','dominate'), ('should','obey'),
                ('should','punish'), ('should', 'respect'), ('stay','in'), ('step','on'),
                ('superior','to'), ('use','force'), ('well','behaved'), ('well','mannered'),
                ('well','raised'), ('win','the'), ('work','ethic'), ('worse','than')]
    
    low_dom  = [('amplified','for'), ('as','equal'), ('ask','questions'), ('be','curious'),
                ('be','equal'), ('be','inclusive'), ('be','independent'), ('be','inquisitive'),
                ('be','open'), ('be','skeptical'), ('be','fair'), ('by','asking'), 
                ('by','listening'), ('by','negotiation'), ('discriminate','against'),
                ('equality','should'), ('equal','chance'), ('equal','rights'), 
                ('equalize','conditions'), ('fair','chance'), ('fair','deal'), 
                ('fair', 'opportunity'), ('free','speech'), ('free', 'thinker'), ('free','thought'),
                ('independent','thought'),('independent','thinker'),
                ('inclusive','of'), ('isolated','case'), ('just','to'),
                ('make','amends'), ('mirrored','by'), ('moral','obligation'), ('more','fair'),
                ('more','equal'), ('more','equitable'), ('more','inclusive'), ('more','just'),
                ('open','discussion'), ('open','mind'), ('open','to'), 
                ('power','in'), ('protecting','the'), ('publicizing','the'), ('respecting','others'),
                ('replicated','for'), ('responsible','for'), ('the','obligation'), ('the','culture'),
                ('the','survivors'), ('the','pervasiveness'), ('the','victims'), ('this','reality'),
                ('through','discussion'), ('through','negotiation'), ('to','equalize'),
                ('to','compensate'), ('to','discriminate'), ('to','negotiate'),
                ('to','prevent'), ('to','rehabilitate'), ('to','restore'), ('to','understand'),
                ('realities','of'), ('same','chance'), ('scale','of'), ('scope','of'),
                ('should','share'), ('streamlining','the'), ('social','equality'),
                ('social','justice'), ('unfair','to'), ('unjust','to')]
    
    
    score    = 0
    keywords = 0
    
    article_text = article_text.lower()
    article_text = article_text.split()
    
    for pair in article_text:
        for word in pair:
            word = nltk.PorterStemmer().stem(word)
    
    for pair in high_dom:
        for word in pair:
            word = nltk.PorterStemmer().stem(word)
    
    for pair in low_dom:
        for word in pair:
            word = nltk.PorterStemmer().stem(word)    
    
    article_text = list(nltk.bigrams(article_text))
    
    for pair in article_text:
        if pair in high_dom:
            score    += 1
            keywords += 1
        elif pair in low_dom:
            score    -= 1
            keywords += 1
    
    if keywords == 0:
        print('No keyword pairs to analyze.')
    elif keywords > 0:    
        if score > 0:
            print('High dominance detected. Score was: ', score/len(article_text))
            print('Keyword pair count was {}.'.format(keywords))
        elif score < 0:
            print('Low dominance detected. Score was: ', score/len(article_text))
            print('Keyword pair count was {}.'.format(keywords))
        elif score == 0:
            print('Neutral dominance detected.')
            print('Keyword pair count was {}.'.format(keywords))
    
main()